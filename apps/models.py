from django.db import models


# Create your models here.
class Contact(models.Model):
    name=models.CharField(max_length=30)
    email=models.EmailField()
    phonenumber=models.CharField(max_length=12)
    description=models.TextField()

    def __str__(self):
        return f'message from {self.name}'
    
class Blogs(models.Model):
    title=models.CharField(max_length=30)
    description=models.TextField()
    authname=models.CharField(max_length=30)
    img=models.ImageField( upload_to=None, height_field=None, width_field=None, max_length=None)
    timestamp=models.DateTimeField( auto_now_add=True)

    def __str__(self):
        return f'messagefrom {self.authname}'