from django.urls import path
from apps import views
urlpatterns = [
   path('',views.home,name='home'),
   path('contact',views.contact_us,name='contact_us'),
   path("about",views.about,name='about'),
   path("services",views.services,name='services'),
   path("blog",views.blog,name='blog'),
   path("Login",views.handlesLogin,name='handlesLogin'),
   path('Logout',views.Logout,name='Logout'),
   path("Signup",views.HandlesSignup,name='HandlesSignup'),
]
